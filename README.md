# Guide d'installation
Ce guide explique brièvemenent le fonctionnement de notre solution, ainsi que la manière de la mettre en place et de l'exécuter. Nous avons inclus sur ce gitlab l'intégralité du code retenu pour ce projet, à savoir le déploiment des ressources cloud, la visualisation ainsi que l'entraînement du script Python de prédiction. Nous avons de plus inclus nos présentations de restitution de sujet et de soutenance finale pour que vous ayez un aperçu total de notre travail.

Suite à l'assemblage du modèle de prédiction des données et de la lambda récupérant des fichiers et les mettant sur un nouveau bucket, nous n'avons pas pu résoudre un problème de dépendance vers sklearn. Notre solution n'est donc pas fonctionnelle pour le moment.  
Pour avoir tout de même accès à la visualisation des predictions, il suffit de mettre les fichiers `cloud_word.csv`, `cloud_word_gender.csv` et `predict.csv` du dossier `script_python/data` sur le bucket de sortie.  

Bonne utilisation, 

Victoria VEGAS, Gabrielle FAURIE, Etienne DUMONTET.

## Prérequis
Notre solution est décomposée en deux parties, une contenant le lien entre Hadoop et le cloud et une autre contenant le lien entre le cloud et la visualisation finale.  
Les deux parties peuvent être hébergées sur des machines différentes.  

Pour faire marcher notre solution vous devez avoir les logiciels suivants installés sur la ou les machines concernées :  
*    Pour le projet Hadoop vers AWS : 
        *    Node.js  
        *    Avoir un compte AWS  
*    Pour le projet AWS vers Visualisation : 
        *    Node.js v12 minimum
        *    MongoDb v4 minimum
        *    Avoir les accès au même compte AWS que celui de la solution Hadoop vers AWS

## Machine Hadoop vers Cloud AWS
La première chose à faire est de se connecter à AWS et de mettre à jour ses credentials (dans le dossier .aws/credentials de votre ordinateur).  

Il faut ensuite placer le dossier aws contenant l'intégralité de la solution dans le répertoire de votre choix. Une fois cette étape complétée, il faut modifier le fichier config.json, situé à la racine de ce dossier, en indiquant les valeurs suivantes :  
*    localFolderPath : le chemin d'accès local au dossier contenant les fichiers .js (bien mettre le séparateur / et non \ entre 2 dossiers)  
*    fileName : le nom du fichier à exporter (ex : "data.json")  
*    Dans la section hadoop :  
        *    filePath : chemin d'accès au fichier sous Hadoop  
        *    host : le nom du serveur faisant marcher hadoop  
        *    username : le nom d'utilisateur utilisé pour se connecter à Hadoop  
        *    password : le mot de passe utilisé pour se connecter à Hadoop  
        *    port : le port sur lequel tourne Hadoop  
*    Dans la section AWS :  
        *    bucketNameIn : le nom du bucket où va être stocké les données d'entrées  

Lancer ensuite une invite de commandes :
1. Se placer à la racine du dossier aws `cd c:/data/aws`
2. Lancer la commande `npm install` pour télécharger les modules manquants
3. Lancer la commande `node .\CreateAWSArchitecture.js` qui va créer toute notre architecture AWS et charger le modèle d'apprentissage sur AWS.
4. Lancer la commande `node .\CreateAWSArchitecture.js` qui va charger les données sur s3. Le fichier data.json étant lourd, cette étape peut prendre plus de 20mins.

Pour faire de nouvelles prédictions, il y a juste à mettre à jour ces credantials et exécuter la commande `node .\CreateAWSArchitecture.js`



## Machine Cloud AWS vers serveur local de visualisation
La première chose à faire est de se connecter à AWS et de mettre à jour ses credentials (dans le dossier .aws/credentials de votre ordinateur).

Il faut ensuite placer le dossier dataviz_web_server contenant l'intégralité de la solution dans le répertoire de votre choix. Une fois cette étape complétée, il faut modifier le fichier config.json, situé à la racine de ce dossier, en indiquant les valeurs suivantes :
*    mongoDPath : le path de l'exécutable mongod.exe, servant à lancer un instance locale de mongodb (uniquement dans le cadre d'une exécution sur Windows)
*    mongoDbConnexionUri : la connexion string permettant d'accéder à l'instance mongo locale
*    awsS3Region : région sur laquelle est située le s3 contenant les fichiers  
*    awsS3OutputBucket : nom du bucket s3 contenant les fichiers 6
*    awsS3PredictPath : sous-dossier contenant le fichier predict.csv  
*    awsS3WordCloudPath : sous-dossier contenant le fichier cloud_word.csv  
*    awsS3WordCloudGenderPath : sous-dossier contenant le fichier cloud_word_gender.csv

Lancer ensuite un invité de commandes :
1. Se placer à la racine du dossier dataviz_web_server
2. Lancer la commande `npm install` pour télécharger les modules manquants
3. Lancer la commande `node app.js` qui mettra en marche le serveur web
4. Se connecter sur le navigateur web de votre choix à l'adresse 'localhost:3000'

Une fois sur cette page, deux options s'offrent à vous :
*   Obtenir les dernières prédictions :
        Appuyer sur ce bouton fera importer les données d'AWS à mongo, en fonction de la dernière date d'import. Avant de lancer cette instruction, assurez-vous que vos credentials sont à jour.
*   Visualiser les données :
        Appuyer sur ce bouton amènera sur la page des visualisations, où deux options sont disponibles : 
    *   Visualiser par CV : des stats sont disponibles pour chaque CV, trouvables par Id_Cv : nuage de mots utiles du CV, et top trois des chances de métier prédits par notre algorithme
    *   Visualiser par métier : pour chaque métier ayant été appris par l'algorithme, nous avons restitué des statistiques globales et en fonction du genre   


## Fichiers présents dans scripts python
Ces fichiers ne sont pas utile pour le fonctionnement global de la solution.
Ils servent juste à vous montrer les différentes étapes afin de construire notre modèle d'apprentissage automatique :
visualisation des données / nettoyage des données / test de différents modèles / méthodes nécessaires pour créer nos fichiers finaux .
 
 Prérequis :
 1. Installer Jupyter Notebook afin de les visualiser ou les ouvrir à l'aide de Google Colab
 
 * lecture_data.ipynb : première lecture des données
 * test_training.ipynb : nettoyage des données et test des différents modèles d'apprentissage et des différents hyperparametres
 * inference.ipynb : fichier final utilisé dans AWS, nettoyage + apprentissage du modèle sur les descriptions des CV
