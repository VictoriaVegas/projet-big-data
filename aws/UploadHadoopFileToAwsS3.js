var AWS = require('aws-sdk');
const fs = require('fs');
const {NodeSSH} = require('node-ssh')
 
const ssh = new NodeSSH();

var s3 = new AWS.S3({
	'region': 'us-east-1',
    httpOptions: {
        timeout: 2400000
    }
});

/////////////////////////////////////// Variables de config.json
const data = require('./config.json');
var hadoopFilePath = data.hadoop.filePath;
let host = data.hadoop.host;
let username = data.hadoop.username;
let port = data.hadoop.port;
let password = data.hadoop.password;
let localFolderPath = data.localFolderPath;
let fileName = data.fileName;

let bucket = data.AWS.bucketNameIn;

let localFilePath = localFolderPath + '/' + fileName;
///////////////////////////////////////////////////

//si chemin du fichier local non renseigné
if (!localFilePath || localFilePath.length < 1) {
    console.error('Missing local file path.'); 
    process.exit(1);
}

//si bucket non renseignée
if (!bucket || bucket.length < 1) {
    console.error('Missing Bucket local file.'); 
    process.exit(1);
}

connectSSH();

function connectSSH() {
    ssh.connect({
    host: host,
    username: username,
    port: port,
    password: password
    }).then(function() {

        putFileInLocalPath()
        .then(() => { 

            readFileAndAddItToS3();
        });
    })
    .catch(function(err) {
        console.error(err, err.stack);
    });
}

function putFileInLocalPath() {
    return ssh.getFile(localFilePath, hadoopFilePath + fileName).then(function() {
        console.log("The file " + fileName + " content is successfully downloaded");
      }, function(error) {
        console.log("Something's wrong with file " + fileName);
        console.log(error);
      });
}

function readFileAndAddItToS3() {
    // Read content from the file
    const fileContent = fs.readFileSync(localFilePath);

    //recupère extension fichier
    const extensionFile = fileName.split('.').pop();

    //paramètres importants pur transfere file to S3 bucket
    let params = {
        Body: fileContent, // contenu du fichier a copier
        Bucket: bucket + "/data", // nom du bucket
        Key: fileName , // nom du fichier
        ACL: 'public-read', //droit du fichier ex :  public-read-write /bucket-owner-read / bucket-owner-full-control
        ContentType: "text/" + extensionFile // type de fichier 
    };

    s3.upload(params, function(err, data) {
        if (err) {// if erreurs
            console.error(err);
        } else { // si pas d'erreurs
            console.log("Fichier " + fileName + " correctement installé dans le bucket S3"); 
        }
    });
}
