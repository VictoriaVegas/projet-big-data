var AWS = require('aws-sdk');
const fs = require('fs');

const data = require('./config.json');
let bucketIn = data.AWS.bucketNameIn;
let bucketOut = data.AWS.bucketNameOut;


//si bucket non renseignée
if (!bucketIn || bucketIn < 1) {
    console.error('Missing bucket name.'); 
    process.exit(1);
}

// Création de la policy
var myPolicy = {
    "Version": "2012-10-17",
    "Statement": [
        {
        "Effect": "Allow",
        "Principal": {
            "Service": "lambda.amazonaws.com"
        },
        "Action": [
            "sts:AssumeRole"
        ]
        }
    ]
};

let roleName = "lambdaRole2";
var roleArn = undefined;
let policyArn = "arn:aws:iam::aws:policy/AdministratorAccess";
let functionName = 'get-model-test-2';

var createIamParams = {
    AssumeRolePolicyDocument: JSON.stringify(myPolicy),
    RoleName: roleName
};

var policyParams = {
    PolicyArn: policyArn,
    RoleName: roleName
};

// Lecture du fichier model.zip dans le dossier courant
const fileContent = fs.readFileSync(__dirname + '/model.zip');
var dependenciesFiles = undefined;

// Paramètre pour ajouter le modèle dans la bucket
let modelZipParams = {
    Body: fileContent, // contenu du fichier a copier
    Bucket: bucketIn + "/model", // nom du bucket
    Key: 'model.zip' , // nom du fichier
    ACL: 'public-read', //droit du fichier ex :  public-read-write /bucket-owner-read / bucket-owner-full-control
    ContentType: "text/" + 'zip' // type de fichier 
};

// Création des services aws
const s3 = new AWS.S3(
    {
        apiVersion: '2006-03-01', 
        'region': 'us-east-1',
        httpOptions: {
            timeout: 240000
        }
    });
const iam = new AWS.IAM({apiVersion: '2010-05-08', 'region': 'us-east-1'});
const lambda = new AWS.Lambda({apiVersion: '2015-03-31', 'region': 'us-east-1'});

createAWSArchi();

function createAWSArchi() {
    try {
        // Création de la bucket où vont être stocké les données
        s3.createBucket({Bucket: bucketIn}).promise();
        s3.waitFor("bucketExists", { Bucket : bucketIn }, function(err, file) {
            if (err) {// if erreurs
                console.error(err);
            } else { // si pas d'erreurs
                console.log("Bucket Exists OK");
                
                // Ajout du modèle de prediction sur le bucket
                s3.putObject(modelZipParams, function(err, file) {
                    manageError(err, "Fichier correctement installé dans le bucket S3");
                    createLambda();
                });
                
                createIamRole();
            }
        });

        // Création de la bucket où vont être stocké les résultats
        s3.createBucket({Bucket: bucketOut}).promise();
        s3.waitFor("bucketExists", { Bucket : bucketOut }, function(err, data) {
            manageError(err, "Bucket OUT Exists  OK");
        });

    } catch (err) {
        console.log(err);
    }
};

function createIamRole() {
    // Création du rôle iam pour la lambda
    iam.createRole(createIamParams).promise();
    iam.waitFor("roleExists", { RoleName: roleName }, function(err, role) {
        if (err) {// if erreurs
            console.error(err);
        } else { // si pas d'erreurs
            console.log("Role Exists OK");
            roleArn = role.Role.Arn;
            attachPolicy();
        }});
}

function attachPolicy() {
    // Création de la policy rattachée au rôle iam
    iam.attachRolePolicy(policyParams, function(err, policy) {
        if (err) {// if erreurs
            console.error(err);
        } else { // si pas d'erreurs
            console.log("Policy Exists OK"); 
        }});
}

function createLambda() {
    // Paramètres utile à la création de lambda
    let lambdaParams = {
        FunctionName: functionName,
        Code: {
            S3Bucket: bucketIn,
            S3Key: 'model/model.zip'
        },
        Runtime: 'python3.8',
        Role: roleArn,
        Handler: 'model/lambda.lambda_handler',
        Timeout: 900, //Timeout max = 15min pour que le modèle puisse tourner
        MemorySize : 10240
    };

    // Création de la lambda
    lambdaCreated = lambda.createFunction(lambdaParams, function(err, data) {
        
        // Test de création une deuxième fois avec time out pour attendre que le rôle soit créé
        if (err && err.code === 'InvalidParameterValueException') {            
            setTimeout(function(){
                lambdaCreated = lambda.createFunction(lambdaParams, function(err, data) {
                    if (err) console.log(err);
                    else {
                        console.log('lambda OK');
                        manageLambdaPermission(lambdaCreated.response.data.FunctionArn);
                    }
                });
            }, 10000);
        } else {
            manageError(err, data);
            if (data) {
                manageLambdaPermission(lambdaCreated.response.data.FunctionArn);
            }
        }
    });
}

function manageLambdaPermission(lambdaArn) {
    const params = {
        Action: 'lambda:InvokeFunction',
        FunctionName: functionName,
        Principal: 's3.amazonaws.com',
        SourceArn : 'arn:aws:s3:::' + bucketIn,
        StatementId: 's3'
    };
    
    lambda.addPermission(params, function (err, data) {
        if (err) {
            console.log(err);
        } else {
            console.log(data);
            addEventS3PutObject(lambdaArn);
        }
    });
}

function addEventS3PutObject(lambdaArn) {
    let Bparams = {
        Bucket: bucketIn, 
        NotificationConfiguration: {
            LambdaFunctionConfigurations: [
              {
                LambdaFunctionArn: lambdaArn,
                Events: ['s3:ObjectCreated:*']
              },
            ]            
        }
    };

    s3.putBucketNotificationConfiguration(Bparams, function(err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else     console.log(data);           // successful response
    });
}

function manageError(err, msg) {
    if (err) {// if erreurs
        console.error(err);
    } else { // si pas d'erreurs
        console.log(msg); 
    }
}
